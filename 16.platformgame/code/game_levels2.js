var GAME_LEVELS = [
    ["                                                                                ",
        "                                                                                ",
        "                                                                                ",
        "                                                                                ",
        "                                                                                ",
        "                                                                                ",
        "                                                                 oxxx           ",
        "                                                   xx      x     xx!xx          ",
        "                                             xx                  x!!!x          ",
        "  x x                                       |            o      xx!xx          ",
        "  xo   x                             xxx                          xvx           ",
        "  x                                                                         xx  ",
        "  x                                      |                                   x  ",
        "  x                     o                                                    x  ",
        "  x          xxxx                        xxxxx                             o x  ",
        "  x          x  x                                                            x  ",
        "  xl @       x  x               |                                xxxxx     = x  ",
        "  x!xxxxxxxxxx  xxxxxxxx  xx      xxxxxxxxxxxxxxxxxxxx     xxxxxxx   xxxxxxxxx  ",
        "                       xl      l lx                  x     x                    ",
        "                       x!!!!!!!!!!x                  x!!!!!x                    ",
        "                       x!!!!!!!!!!x                  x!!!!!x                    ",
        "                       xxxxxxxxxxxx                  xxxxxxx                    ",
        "                                                                                ",
        "                                                                                "
    ],
    ["                                         x!!x                                                                   x!x  ",
        "                                    ||x!!x                                                                   x!x  ",
        "                                      x!!xxxxxxxxxxx                                                         x!x  ",
        "                                      xx!!!!!!!!!!!!f                                                        x!x  ",
        "                                       xxxxxxxxxx!!x                                              o   o   o  x!x  ",
        "                                                xx!x               o   o                                    xx!x  ",
        "                                                 x!x                                          xxxxxx|xxx|xxxx!!x  ",
        "                                                 xvx               x   x           =            !!!!!!!!!!!!!!!x  ",
        "                                                             xx  |   |   |  xx            xxxxxxxxxxxxxxxxxxxx!x  ",
        "                                                              xx!!!!!!!!!!!xx            v                    v   ",
        "     @                                                         xxxx!!!!!xxxx                                      ",
        "                                                                  xvxxxvx        xxx                     x        ",
        "                                                                                 x x                     x        ",
        "                                                                                   x                    xxx       ",
        "                                                                                   xx                    x        ",
        "                                                                                   x       x      x      x        ",
        "                                                            o o      x   x         x                     x        ",
        "                                                                     x   x         x                     xx       ",
        "                                                                     x   x   x!xxxxxxxx                  x        ",
        "                                                           x!x!x     x   x   xv   =                      x        ",
        "             x   !x!   x                                l            x   x   x                           x        ",
        "    x!x      x         x                                             x   xxxxx   xxxxxx                  xxxxx x  ",
        "    x x      x         x       x!x x!x                               x     o     x x                           x  ",
        "!!!!x x!!!!!!x         x!!!!!!xx     xx!!!!!!!!xxx xxx!!!!!!!!!!     x          =x x            =              x  ",
        "!!!!x x!!!!!!x         x!!!!!xx       xxxxxxxxxx     x!!!!!!!xx!     xxxxxxxxxxxxx xx  o o  xxxxxxxx    xxxxxxxx  ",
        "!!!!x x!!!!!!x         x!!!!!x    o                 xx!!!!!!xx !                    xx     xx      x!!!!x         ",
        "!!!!x x!!!!!!x         x!!!!!!f                     x!!!!!!xx  !                     xxxxxxx                      ",
        "!!!!x x!!!!!!x         x!!!!!xx       xxxxxxxxxxxxxxx!!!!!xx   !                                                  ",
        "!!!!x x!!!!!!x         x!!!!!!xxxx!xxxx!!!!!!!!!!!!!!!!!!xx    !   l                                              ",
        "!!!!x x!!!!!!x         x!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!xx     !!!!!!                                             "
    ],
        ["                                                                                                              ",
            "                                                                                                              ",
            "                                                                                                              ",
            "                                                                                                              ",
            "                                                                                                              ",
            "                                        o                                                                     ",
            "                                                                                                              ",
            "                                        x                                                                     ",
            "                                        x                                                                     ",
            "                                        x                                                                     ",
            "                                        x                                                                     ",
            "                                       xxx                                                                    ",
            "                                       x x                 !!!        !!!  xxx                                ",
            "                                       x x                 !x!        !x!                                     ",
            "                                     xxx xxx                x          x                                      ",
            "                                      x!!!x                 x   oooo   x       xxx                            ",
            "                                      x!!!x                 x          x      x!!!x                           ",
            "                                      x!!!x                 xxxxxxxxxxxx       xxx                            ",
            "                                     xx!!!xx        xxx      x                                                ",
            "                                      x!!!x           x!x!x!xx              x x                               ",
            "                                      x!!!x           x                    x!!!x                              ",
            "                                      x!!!x           x                     xxx                               ",
            "                                     xx!!!xx          x                                                       ",
            "                                      x!!!x= = = =    x            xxx                                        ",
            "                                      x!!!x           x           x!!!x                                       ",
            "                                      x!!!x    = = = =x     o      xxx       xxx                              ",
            "                                     xx!!!xx          x                     x!!!x                             ",
            "                              o   o   x!!!x           x     x                xxv        xxx                   ",
            "                                      x!!!x           x              x                 x!!!x                  ",
            "                             xxx xxx xxxvxxx     o o  x!!!!!!!!!!!!!!x                   vx                   ",
            "                             x xxx x x x x x          x!!!!!!!!!!!!!!x                                        ",
            "                             x             x   xxxxxxxxxxxvxxxxxxxvxxx                                        ",
            "                             xx           xx                                         xxx                      ",
            "  xxx                         x     x     x                                         x!!!x                xxx  ",
            "  x x                         x    xxx    x                                          xxx                 x x  ",
            "  x                           x    xxx    xxxxxxx                        xxxxx                             x  ",
            "  x                          |x        =  x                              x   x                             x  ",
            "  x                           xx          x                              x x x                             x  ",
            "  x                                       x       |xxxx|    |xxxx|     xxx xxx                             x  ",
            "  x                xxx             o o    x                              x         xxx                     x  ",
            "  x               xxxxx       xx          x                             xxx       x!!!x          x         x  ",
            "  x               oxxxo       x    xxx    x                             x x        xxx          xxx        x  ",
            "  x                xxx        xxxxxxxxxxxxx  x oo x    x oo x    x oo  xx xx                    xxx        x  ",
            "  x      @       =  x  =      x           x!!x    x!!!!x    x!!!!x    xx   xx                    x         x  ",
            "  xxxxxxxxxxxxxxxxxxxxxxxxxxxxx           xxxxxxxxxxxxxxxxxxxxxxxxxxxxx     xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ",
            "                                                                                                              ",
            "                                                                                                              "
        ],
        ["                                                                                                  xxx x       ",
            "                                                                                                      x       ",
            "                                                                                                  xxxxx       ",
            "                                                                                                  x           ",
            "                                                                                                  x xxx       ",
            "                          o                                                                       x x x       ",
            "                                                                                             o o oxxx x       ",
            "                   xxx                                                                                x       ",
            "       !  o  !                                                xxxxx xxxxx xxx x xxx x xxxxx xxx x xxxxx       ",
            "       x     x                                                x   x x   x x   x x   x x   x x   x x           ",
            "       x= o  x            x                                   xxx x xxx x xxx x xxx x xxx x xxx x xxxxx       ",
            "       x     x                                                  x x!!!x x!!!x x!!!x x!!!x x!!!x x=    x       ",
            "       !  o  !            o                                  xxxx xxxxx xxxxx xxxxx xxxxx xxxxx xxxxxxx       ",
            "                                                                                                              ",
            "          o              x x                               x                                                  ",
            "                          |                                                                                   ",
            "                                                                                                              ",
            "                                                       x                                                      ",
            "                   xxx         xxx                                                                            ",
            "                                                                                                              ",
            "                          o                                                     x      x                      ",
            "                                                          xx     xx                                           ",
            "             xxx         xxx         xxx                                 x                  x                 ",
            "                                                                                                              ",
            "                                                                 ||                                           ",
            "  xxxxxxxx!xx                                                                                                 ",
            "  x         x o xxxxxxxxx o xxxxxxxxx o xx                                                x                   ",
            "  x         x   x       x   x       x   x                 ||                  x     x                         ",
            "  x@        x xxx   o   xxxxx   o   xxxxx                                                                     ",
            "  xxxxxxx                                     xxxxx       xx     xx     xxx                                   ",
            "        x=                  =                =x   x                     xxx                                   ",
            "        xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   x!!!!!!!!!!!!!!!!!!!!!xxx!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",
            "                                                  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
            "                                                                                                              "
        ]
];

if (typeof module != "undefined" && module.exports)
    module.exports = GAME_LEVELS;